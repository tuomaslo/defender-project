﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothCameraFollow : MonoBehaviour {

	public Transform player;
	public bool useOffsetTresholds = false;
	public Vector3 offsetTresholdMax;
	public Vector3 offsetTresholdMin;
	public Vector3 cameraOffset;
	public bool followX,followY;
	public float smoothTime;
	private bool changeInX,changeInY;
	private Vector3 offset;
	private Vector2 velocity;
	private bool turning = false;
	private bool endLerp = false;
	private float turnTimer = 0;
	private float lerpTime = 0f;
	public float lerpSpeed = 1.0f;
	void FixedUpdate()
	{
		offset = transform.position - player.position;
		
		Vector3 newCameraPosition = transform.position;
		if(followX)
		{
			if(useOffsetTresholds)
			{
				if (offset.x > offsetTresholdMax.x)
				{
					changeInX = true;
					newCameraPosition.x = (player.position.x + offset.x) - (offset.x - offsetTresholdMax.x);
				}
				else if (offset.x < offsetTresholdMin.x)
				{
					changeInX = true;
					newCameraPosition.x = (player.position.x + offsetTresholdMin.x);
				}
			}
			else
			{
				changeInX = true;
				newCameraPosition.x = player.position.x + cameraOffset.x;
			}
		}
		
		if(followY)
		{
			if(useOffsetTresholds)
			{
				if (offset.y > offsetTresholdMax.y)
				{
					changeInY = true;
					newCameraPosition.y = (player.position.y + offset.y) - (offset.y - offsetTresholdMax.y);
				}
				else if (offset.y < offsetTresholdMin.y)
				{
					changeInY = true;
					newCameraPosition.y = (player.position.y + offset.y) - (offset.y + offsetTresholdMin.y);
				}
			}
			else
			{
				newCameraPosition.y = player.position.y + cameraOffset.y;
			}
		}

		if (!changeInX)
		{
			newCameraPosition.x = transform.position.x;
		}
		if (!changeInY)
		{
			newCameraPosition.y = transform.position.y;
		}

		if(useOffsetTresholds || turning)
		{
			float smoothX = Mathf.SmoothDamp(transform.position.x, newCameraPosition.x, ref velocity.x, smoothTime);
			float smoothY = Mathf.SmoothDamp(transform.position.y, newCameraPosition.y, ref velocity.y, smoothTime);
			transform.position = new Vector3(smoothX,smoothY, transform.position.z);
		}
		else if(endLerp)
		{
			float smoothX = Mathf.Lerp(transform.position.x, newCameraPosition.x, lerpTime);
			float smoothY = Mathf.Lerp(transform.position.y, newCameraPosition.y, lerpTime);
			transform.position = new Vector3(smoothX,smoothY, transform.position.z);

			lerpTime += lerpSpeed * Time.deltaTime;
			
			if(lerpTime>1f)
			{
				endLerp = false;
				transform.position = newCameraPosition;
			}
		}
		else
		{
			transform.position = newCameraPosition;
		}

		if(turning)
		{
			turnTimer-=Time.deltaTime;
			if(turnTimer<=0)
			{
				turning = false;
				endLerp = true;
				lerpTime = 0f;
			}
		}
		
	}

	public void Reverse()
	{
		cameraOffset.x *= -1;
		turning = true;
		turnTimer = smoothTime*3f;
	}
}
