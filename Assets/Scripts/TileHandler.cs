﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileHandler : MonoBehaviour {

	private Transform playerObject;
	public List<GameObject> tiles = new List<GameObject>();
	public List<Vector3> originalPositions = new List<Vector3>();
	private int currentTile = 0;
	public float treshold = 10f;
	private bool nextLoaded = false;
	private bool prevLoaded = false;
	private EnemyHandler enemyHandler;
	private void Start()
	{
		foreach(Transform t in transform)
		{
			tiles.Add(t.gameObject);
			originalPositions.Add(t.position);
		}
		for(int i = 1;i<tiles.Count;i++)
		{
			tiles[i].SetActive(false);
		}
		playerObject = GameObject.Find("PlayerObject").transform;
		enemyHandler = GameObject.Find("EnemyHandler").GetComponent<EnemyHandler>();
	}

	public void RestartLevel()
	{
		for(int i = 0;i<tiles.Count;i++)
		{
			tiles[i].SetActive(true);
			tiles[i].transform.position = originalPositions[i];
		}
		for(int i = 1;i<tiles.Count;i++)
		{
			tiles[i].SetActive(false);
		}
		currentTile = 0;
	}
	private void Update()
	{
		float dist = playerObject.position.x-tiles[currentTile].transform.position.x;
		if(dist>treshold)
		{
			if(!nextLoaded)
				LoadNext();
			if(dist>275)
			{
				if(currentTile==tiles.Count-1)
					currentTile = 0;
				else
					currentTile = currentTile+1;
				UnloadPrev();
			}
		}
		else if(dist<-treshold)
		{
			if(!prevLoaded)
				LoadPrev();
			if(dist<-275)
			{
				if(currentTile == 0)
				{
					currentTile = tiles.Count-1;
				}
				else
				{
					currentTile = currentTile-1;
				}
				UnloadNext();
			}
		}
	}

	private void LoadNext()
	{
		nextLoaded = true;
		int next;
		if(currentTile==tiles.Count-1)
		{
			next = 0;
		}
		else
		{
			next = currentTile+1;			
		}
		tiles[next].transform.position = new Vector3(tiles[currentTile].transform.position.x+300f,tiles[next].transform.position.y,tiles[next].transform.position.z);
		tiles[next].SetActive(true);
		if(prevLoaded)
			UnloadPrev();
		
	}

	private void UnloadPrev()
	{
		prevLoaded = false;
		int prev;
		if(currentTile == 0)
		{
			prev = tiles.Count-1;
		}
		else
		{
			prev = currentTile-1;
		}
		enemyHandler.UpdateEnemyTiles();
		tiles[prev].SetActive(false);
	}
	private void LoadPrev()
	{
		prevLoaded = true;
		int prev;
		if(currentTile == 0)
		{
			prev = tiles.Count-1;
		}
		else
		{
			prev = currentTile-1;
		}
		tiles[prev].transform.position = new Vector3(tiles[currentTile].transform.position.x-300f,tiles[prev].transform.position.y,tiles[prev].transform.position.z);
		tiles[prev].SetActive(true);
		if(nextLoaded)
			UnloadNext();
	}

	private void UnloadNext()
	{
		nextLoaded = false;
		int next;
		if(currentTile==tiles.Count-1)
		{
			next = 0;
		}
		else
		{
			next = currentTile+1;			
		}
		enemyHandler.UpdateEnemyTiles();
		tiles[next].SetActive(false);
	}

	


		
	
}
