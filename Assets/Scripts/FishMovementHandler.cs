﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishMovementHandler : MonoBehaviour {

	public float speedMin,speedMax;
	private float speed;
    private float frequency = 20.0f; 
	 public float frequencyMin,frequencyMax;
     public float magnitudeMin,magnitudeMax;
	 private float magnitude; 
     private Vector3 axis;
 
     private Vector3 pos;
 
     void Start () {
		 transform.Rotate(0,180,0);
         pos = transform.position;
		 speed = Random.Range(speedMin,speedMax);
		 frequency = Random.Range(frequencyMin,frequencyMax);
		 magnitude = Random.Range(magnitudeMin,magnitudeMax);
         axis = transform.up;
     }
     
     void Update () {
         pos += transform.right * Time.deltaTime * speed;
		 Vector3 newPos = pos + axis * Mathf.Sin (Time.time * frequency) * magnitude;
		 Vector3 direction = transform.position + newPos;
		 transform.position = newPos;
	 }
}
