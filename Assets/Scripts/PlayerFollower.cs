﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollower : MonoBehaviour {

	public Transform playerToFollow;
	public bool matchX,matchY;
	// Update is called once per frame
	private void FixedUpdate()
	{
		Vector3 pos = transform.position;
		if(matchX)
			pos.x = playerToFollow.position.x;
		if(matchY)
			pos.y = playerToFollow.position.y;
		transform.position = pos;	
	}
}
