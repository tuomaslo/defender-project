﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileHandler : MonoBehaviour {

	public float speed = 1.0f;
	public bool enemyProjectile = false;
	private bool referencesSet = false;
	private Rigidbody2D rigidbody2d;
	private float destroyTimer = 4f;
	private bool playerShot = true;
	private ScoreHandler scoreHandler;
	private Vector3 direction;
	private GameObject explosionPrefab;
	
	
	void Start () {
		if(!referencesSet)
			GetReferences();
	}

	public void StartMoving(Vector3 direction)
	{
		if(!referencesSet)
			GetReferences();
		this.direction = direction;
		rigidbody2d.AddForce(new Vector2(direction.x,direction.y)*speed);
	}
	
	private void GetReferences()
	{
		rigidbody2d = GetComponent<Rigidbody2D>();
		scoreHandler = GameObject.Find("ScoreHandler").GetComponent<ScoreHandler>();
		explosionPrefab = transform.GetChild(1).gameObject;
		referencesSet = true;
	}

	private void Update()
	{
		destroyTimer-=Time.deltaTime;
		if(destroyTimer<0)
			Destroy();
	}

	private void Destroy()
	{
		explosionPrefab.transform.SetParent(null);
		explosionPrefab.SetActive(true);
		Destroy(explosionPrefab,2f);
		Destroy(gameObject);
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		switch(other.transform.tag)
		{
			case "Destroy":
				Destroy();
				break;
			case "Enemy":
				if(!enemyProjectile)
				{
					scoreHandler.AddToScore(10);
					other.GetComponent<EnemyBehaviour>().Explode();
				}
				Destroy();
				break;
			case "Player":
				if(enemyProjectile)
				{
					other.GetComponent<PlayerMovementHandler>().TakeDamage(10);
					Destroy();
				}
				break;
			default: 
				//Debug.Log("Hit "+other.transform.name);
				break;
		}
	}
}
