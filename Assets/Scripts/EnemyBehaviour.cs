﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {

	public enum Type
	{
		normal
	}
	public Type type;
	public float normalEnemyShootInterval = 2.0f;
	public float normalEnemyMovementForce;
	public float playerDetectionRange = 50f;
	public float rotationSpeed = 1.0f, lerpTime = 1.0f;
	public GameObject explosionParticles;
	private float timer;
	private ProjectileShooter projectileShooter;
	private Rigidbody2D rigidbody2d;
	private Transform playerTransform;
	private bool playerDetected = false, playerLocked = false;
	public Light spotLight;
	private Quaternion targetRotation; 
	private EnemyHandler enemyHandler;
	public GameObject healthDrop;

	void Start () {
		projectileShooter = GetComponent<ProjectileShooter>();
		rigidbody2d = GetComponent<Rigidbody2D>();
		playerTransform = GameObject.Find("PlayerObject").transform;
		spotLight.color = Color.green;
		enemyHandler = GameObject.Find("EnemyHandler").GetComponent<EnemyHandler>();

		if(Random.Range(0,2)==1)
			TurnAround();
	}

	private void TurnAround()
	{
		transform.Rotate(0f,180f,0f);
	}
	
	public void Explode()
	{
		explosionParticles.transform.SetParent(null);
		explosionParticles.SetActive(true);
		enemyHandler.EnemyDestroyed(gameObject);
		if(Random.Range(0,10)==0)
		{
			
			GameObject hDrop = Instantiate(healthDrop,transform.position,Quaternion.Euler(0f,0f,Random.Range(0,180f)));
			hDrop.transform.SetParent(null);
		}
		Destroy(explosionParticles,3f);
		Destroy(gameObject);
	}

	private void FixedUpdate()
	{
		if(playerDetected)
		{
			if(playerLocked)
			{
				targetRotation = Quaternion.LookRotation(playerTransform.position - transform.position);
				targetRotation *= Quaternion.Euler(0, 90, 0);
				transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.fixedDeltaTime * rotationSpeed);

				rigidbody2d.AddForce(-transform.right*normalEnemyMovementForce/3f);

				timer+=Time.fixedDeltaTime;

				switch(type)
				{
					case Type.normal:
					if(timer>normalEnemyShootInterval)
					{
						projectileShooter.Shoot(transform.GetChild(0).forward);
						timer = 0;
					}
					break;
				}

				if(Vector3.Distance(playerTransform.position,transform.position)>playerDetectionRange)
				{
					playerLocked = false;
					targetRotation = Quaternion.identity;
					
					if(Random.Range(0,2)==1)
						targetRotation *= Quaternion.Euler(0,180,0);	

					spotLight.color = Color.yellow;	
				}
			}
			else
			{
				//targetRotation *= Quaternion.Euler(0, 90, 0);
				transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.fixedDeltaTime * rotationSpeed);
				timer+=Time.fixedDeltaTime;
				
				if(Quaternion.Angle(targetRotation,transform.rotation)<1f)
				{
					transform.rotation = targetRotation;
					playerDetected = false;
					spotLight.color = Color.green;
				}
			}
		}
		else
		{
			if(transform.position.y>-2 || transform.position.y<-26)
			{
				transform.position = new Vector3(transform.position.x,-15f,0f);
				transform.rotation = Quaternion.identity;
				if(Random.Range(0,2)==1)
						transform.rotation *= Quaternion.Euler(0,180,0);	
			}
			rigidbody2d.AddForce(-transform.right*normalEnemyMovementForce);
			if(Vector3.Distance(playerTransform.position,transform.position)<playerDetectionRange)
			{
				playerDetected = true;
				playerLocked = true;
				spotLight.color = Color.red;
				timer = normalEnemyShootInterval;
			}
		}
	}



}
