﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementHandler : MonoBehaviour {

	[System.Serializable]
	public class Boundary
	{
		public float xMin, xMax, yMin, yMax;
	}

	public float speed;
    public float tilt;
    public Boundary boundary;
	private Rigidbody2D playerRigidbody;
	
    private MobileInputController leftJoystic;
    private bool referencesSet = false;
    public Transform referencePoint;
    public Vector2 constantMovement;
    private ScoreHandler scoreHandler;
    private HealthHandler healthHandler;
    private bool goingRight = true;
    public SmoothTurner smoothTurner;
    public SmoothCameraFollow smoothCameraFollow;
	private void Start()
	{
        if(!referencesSet)
        {
            GetReferences();
        }
	}
    void FixedUpdate ()
    {
        float horizontalMovement = leftJoystic.horizontal;
        float verticalMovement = leftJoystic.vertical;
		Vector2 movementInput = new Vector3 (horizontalMovement, verticalMovement);
       // Debug.Log(movementInput);
        playerRigidbody.AddForce(movementInput * speed);
        playerRigidbody.AddForce(constantMovement);       
        playerRigidbody.position = ClampPosition();
        HandleRotation();
        HandleTurning();
    }

    private void HandleRotation()
    {
        playerRigidbody.rotation = playerRigidbody.velocity.y * tilt;//Quaternion.Euler (0.0f, 0.0f, playerRigidbody.velocity.y *tilt);
    }

    private void HandleTurning()
    {   
        if(goingRight)
        {
            if(playerRigidbody.velocity.x<-25)
            {
                smoothTurner.TurnAround();
                smoothCameraFollow.Reverse();
                goingRight = false;
            }
        }
        else
        {
            if(playerRigidbody.velocity.x>20)
            {
                smoothTurner.TurnAround();
                smoothCameraFollow.Reverse();
                goingRight = true;
            }
        }
    }
    private Vector3 ClampPosition()
    {
        Vector2 refPoint = new Vector2(referencePoint.position.x,referencePoint.position.y);
        return new Vector2
        (
            Mathf.Clamp(playerRigidbody.position.x, refPoint.x+boundary.xMin, refPoint.x+boundary.xMax), Mathf.Clamp(playerRigidbody.position.y, refPoint.y+boundary.yMin, refPoint.y+boundary.yMax)
        );
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        switch(other.transform.tag)
        {
            case "Enemy":
                scoreHandler.AddToScore(-5);
                healthHandler.AddToHealth(-10);
                other.gameObject.GetComponent<EnemyBehaviour>().Explode();
                break;
            case "Collectible":
                healthHandler.AddToHealth(10);
                scoreHandler.AddToScore(5);
                Destroy(other.gameObject);
                break;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        switch(other.transform.tag)
        {
            case "Enemy":
                scoreHandler.AddToScore(-5);
                TakeDamage(10);
                other.GetComponent<EnemyBehaviour>().Explode();
                break;
            case "Collectible":
                healthHandler.AddToHealth(20);
                scoreHandler.AddToScore(5);
                Transform hearts = other.transform.GetChild(1);
                hearts.SetParent(null);
                hearts.gameObject.SetActive(true);
                Destroy(hearts.gameObject,4f);
                Destroy(other.gameObject);
                break;
        }
    }

    public void TakeDamage(int damage)
    {
        healthHandler.AddToHealth(-damage);
    }


    private void GetReferences()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        leftJoystic = GameObject.Find("UI/Controls/LeftJoystic").GetComponent<MobileInputController>();
        scoreHandler = GameObject.Find("ScoreHandler").GetComponent<ScoreHandler>();
        healthHandler = GameObject.Find("HealthHandler").GetComponent<HealthHandler>();
        referencesSet = true;
    }
}
