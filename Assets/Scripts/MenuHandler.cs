﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuHandler : MonoBehaviour {

	public GameObject menuObject, blurObject;
	public void OpenMenu()
	{
		menuObject.SetActive(true);
		blurObject.SetActive(true);
		Time.timeScale = 0f;
	}

	public void CloseMenu()
	{
		
		menuObject.SetActive(false);
		blurObject.SetActive(false);
		Time.timeScale = 1.0f;
	}

	public void Restart()
	{
		SceneManager.LoadScene(0);
	}

	public void Quit()
	{
		Application.Quit();
	}
}
