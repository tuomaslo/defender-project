﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthHandler : MonoBehaviour {

	public float startingHealth = 100f;
	private float health;
	public Slider healthSlider;

	public GameObject playerObject;
	public GameObject playerExplosion;
	public ParticleSystem hitSystem;

	private LevelHandler levelHandler;
	private bool isOver = false;
	private void Start()
	{
		health = startingHealth;
		levelHandler = GameObject.Find("LevelHandler").GetComponent<LevelHandler>();
	}
	public void AddToHealth(int i)
	{
		if(!isOver)
		{
			if(i<0)
			hitSystem.Play();
			health+=(float)i;
			if(health>startingHealth)
				health = startingHealth;
			if(health<=0)
			{
				GameOver();
			}
			UpdateSlider();
		}
	}

	private void GameOver()
	{
		isOver = true;
		playerExplosion.transform.SetParent(null);
		playerExplosion.SetActive(true);
		playerObject.GetComponent<MeshHider>().Hide();
		playerObject.GetComponent<PlayerMovementHandler>().enabled = false;
		StartCoroutine(ShowMenu());
		Destroy(playerExplosion,5f);
	}

	IEnumerator ShowMenu()
	{
		yield return new WaitForSeconds(3f);
		levelHandler.ShowGameOver();
	}



	private void UpdateSlider()
	{
		healthSlider.value = health/startingHealth;
	}
}
