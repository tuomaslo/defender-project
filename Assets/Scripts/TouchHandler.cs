﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class TouchHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	public UnityEvent pointerDownEvent = new UnityEvent();
	public UnityEvent pointerUpEvent = new UnityEvent();
	public bool pointerDown = false;
	
	void Start () {

	}


	
	public void OnPointerDown(PointerEventData eventData)
	{
		pointerDown = true;
		pointerDownEvent.Invoke();
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		pointerDown = false;
		pointerUpEvent.Invoke();
	}
}


