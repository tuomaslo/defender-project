﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OceanFloorHandler : MonoBehaviour {

	public GameObject tilePrefab;
	public float distanceToSpawnNew;
	
	public Transform playerObject;
	private GameObject furthestTile, oldestTile;

	private void Start()
	{
		furthestTile = transform.GetChild(0).gameObject;
	}
	private void Update()
	{
		float dist = furthestTile.transform.position.x-playerObject.position.x;
		if(dist<distanceToSpawnNew)
		{
			SpawnNew();
		}
	}

	private void SpawnNew()
	{
		if(oldestTile!=null)
			DestroyOldest();
		oldestTile = furthestTile;
		furthestTile = Instantiate(tilePrefab, oldestTile.transform.position+new Vector3(500f,0,0f),Quaternion.Euler(90f,0f,0f));;
		
	}

	private void DestroyOldest()
	{
		Destroy(oldestTile);
	}
}
