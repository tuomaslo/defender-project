﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantMovement : MonoBehaviour {

	public Vector2 movementVector;
	private Rigidbody2D rigidbody2d;
	private void Start()
	{
		rigidbody2d = GetComponent<Rigidbody2D>();
	}
	// Update is called once per frame
	private void FixedUpdate()
	{
		rigidbody2d.AddForce(movementVector);   
	}
}
