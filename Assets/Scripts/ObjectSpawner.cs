﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour {

	public List<GameObject> objectsToSpawn = new List<GameObject>();
	public float minSpawnInterval, maxSpawnInterval;
	public Vector3 offsetMin,offsetMax;
	public float minSize = 1f, maxSize = 1f;
	public bool randomRotation = false;
	public bool allAxisRotation = false;
	private float timer;

	// Update is called once per frame
	void Update () {
		timer-=Time.deltaTime;	
		if(timer<=0)
		{
			Spawn();
			timer = Random.Range(minSpawnInterval,maxSpawnInterval);
		}
	}

	private void Spawn()
	{
		GameObject spawned = Instantiate(objectsToSpawn[(int)Random.Range(0,objectsToSpawn.Count-1)],transform.position+new Vector3(Random.Range(offsetMin.x,offsetMax.x),Random.Range(offsetMin.y,offsetMax.y),Random.Range(offsetMin.z,offsetMax.z)),Quaternion.identity);
		if(randomRotation)
		{
			if(allAxisRotation)
				spawned.transform.rotation = Random.rotation;
			else
				spawned.transform.Rotate(new Vector3(0f,Random.Range(0,360),0f));
		}
			
		Vector3 scale = new Vector3(Random.Range(minSize,maxSize),Random.Range(minSize,maxSize),Random.Range(minSize,maxSize));
		spawned.transform.localScale = scale;
	}
}
