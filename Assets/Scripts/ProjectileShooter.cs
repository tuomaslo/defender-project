﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProjectileShooter : MonoBehaviour {

	public Button shootButton;
	public GameObject projectileToShoot;
	public Transform locationToSpawn;
	public float cooldownTime = 0.5f;
	private bool referencesSet = false;
	private float timer;
	private bool onCooldown = false;
	private bool continousShooting = false;
	
	// Use this for initialization
	void Start () {
		if(referencesSet)
			GetReferences();
		if(transform.tag!="Enemy")
			AddListeners();
	}

	// Update is called once per frame
	void Update () {
		if(onCooldown)
		{
			timer+=Time.deltaTime;
			if(timer>=cooldownTime)
			{
				shootButton.image.fillAmount = 1.0f;
				onCooldown = false;
				timer = 0;
				if(continousShooting)
				{
					ShootNormal();
				}
			}
			else
			{
				shootButton.image.fillAmount = timer/cooldownTime;
			}
		}
	}

	public void Shoot(Vector3 direction)
	{
		GameObject tempProjectile = Instantiate(projectileToShoot,locationToSpawn.position,Quaternion.identity,null);
		tempProjectile.GetComponent<ProjectileHandler>().StartMoving(direction);
	}

	private void ShootNormal()
	{
		if(!onCooldown)
		{
			Shoot(transform.GetChild(0).forward);
			onCooldown = true;
			timer = 0;
		}
	}

	private void StartContinuousShooting()
	{
		continousShooting = true;
		ShootNormal();
	}

	private void StopContinuousShooting()
	{
		continousShooting = false;
	}

	private void AddListeners()
	{
		//shootButton.onClick.AddListener(ShootNormal);
		shootButton.GetComponent<TouchHandler>().pointerDownEvent.AddListener(StartContinuousShooting);
		shootButton.GetComponent<TouchHandler>().pointerUpEvent.AddListener(StopContinuousShooting);
		
	}

	private void GetReferences()
	{
		referencesSet = true;
	}


}
