﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceBasedDestroyer : MonoBehaviour {


	private float timer = 0;
	public float intervalToCheckDistance = 5f;
	public float distanceToDestroy = -250f;
	private Transform objectToCheckDistance;
	// Use this for initialization
	void Start () {
		objectToCheckDistance = GameObject.Find("PlayerObject").transform;
	}
	
	// Update is called once per frame
	void Update () {
		timer+=Time.deltaTime;
		if(timer>intervalToCheckDistance)
		{
			CheckDistance();
			timer = 0;
		}
	}

	private void CheckDistance()
	{
		float xDist = transform.position.x-objectToCheckDistance.position.x;
		if(xDist<distanceToDestroy)
		{
			Destroy(gameObject);
		}
	}
}
