﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LevelHandler : MonoBehaviour {

	public int currentLevel = 0;
	public List<int> enemiesPerLevel = new List<int>();

	public TextMeshProUGUI levelTitleText, levelInfoText;
	private EnemyHandler enemyHandler;
	private TileHandler tileHandler;
	public Button continueButton,restartButton;
	private Transform playerTransform, playerFollower;
	
	private void Start()
	{
		enemyHandler = GameObject.Find("EnemyHandler").GetComponent<EnemyHandler>();
		tileHandler = GameObject.Find("EnvTiles").GetComponent<TileHandler>();
		playerTransform = GameObject.Find("PlayerObject").transform;
		playerFollower = GameObject.Find("ObjectsToFollowPlayer").transform;
		ShowFirstLevelInfo();
		restartButton.enabled = false;
		Time.timeScale = 0.0f;
	}

	public void ShowFirstLevelInfo()
	{
		currentLevel++;
		levelTitleText.text = "Level "+currentLevel;
		levelInfoText.text = "Welcome to <b>Sub defender</b>\nYour mission is to destroy all foreing vessels in these seas!";
	}

	public void ShowLevelInfo()
	{
		
		currentLevel++;
		levelTitleText.text = "Level "+currentLevel;
		levelInfoText.text = "Ready for next challenge?";
	}

	public void Continue()
	{
		if(enemyHandler.enemyAmount==0)
			StartNextLevel();
	}

	public void ShowGameOver()
	{
		levelTitleText.text = "Level "+currentLevel;
		levelInfoText.text = "Game over!";
		continueButton.enabled = false;
	}

	public void StartNextLevel()
	{
		restartButton.enabled = true;
		ResetPlayer();
		tileHandler.RestartLevel();		
		enemyHandler.enemyAmount = enemiesPerLevel[currentLevel];
		enemyHandler.SpawnEnemies();
	}

	private void ResetPlayer()
	{
		playerTransform.position = new Vector3(0,-10,0);
		playerFollower.position = Vector3.zero;
		playerTransform.rotation = Quaternion.identity;
		playerTransform.GetComponent<Rigidbody2D>().velocity = new Vector2(0f,0f);
	}
}
