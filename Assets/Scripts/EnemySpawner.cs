﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

	public GameObject enemyPrefab;
	public float maxHeight,minHeight;
	public float maxTime, minTime;
	
	private float timer;
	private float nextTime;

	void Update () {
		timer+=Time.deltaTime;
		if(timer>nextTime)
		{
			SpawnEnemy();
			timer = 0;
			nextTime = Random.Range(minTime,maxTime);
		}	
	}

	void SpawnEnemy()
	{
		GameObject enemy = Instantiate(enemyPrefab,new Vector3(transform.position.x,Random.Range(minHeight,maxHeight),0),Quaternion.identity);

	}
}
