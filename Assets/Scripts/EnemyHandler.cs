﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnemyHandler : MonoBehaviour {

	public int enemyAmount = 0;
	private List<GameObject> enemyList = new List<GameObject>();
	public TileHandler tileHandler;
	public float levelWidth = 600;
	public float levelHeight = -20;
	public GameObject enemyPrefab;
	public TextMeshProUGUI enemiesText;
	private MenuHandler menuHandler;
	private LevelHandler levelHandler;
	private Transform playerTransform;
	private void Start()
	{
		tileHandler = GameObject.Find("EnvTiles").GetComponent<TileHandler>();
		menuHandler = GameObject.Find("MenuHandler").GetComponent<MenuHandler>();
		levelHandler = GameObject.Find("LevelHandler").GetComponent<LevelHandler>();
		playerTransform = GameObject.Find("PlayerObject").transform;
		StartCoroutine(LateStart());
	}

	public void EnemyDestroyed(GameObject enemy)
	{
		enemyList.Remove(enemy);
		enemyAmount--;
		enemiesText.text = ""+enemyAmount;
		if(enemyAmount<=0)
		{
			levelHandler.ShowLevelInfo();
			menuHandler.OpenMenu();
		}
	}

	IEnumerator LateStart()
	{
		yield return null;
	}

	public void SpawnEnemies()
	{
		for(int i = 0;i<enemyAmount;i++)
		{
			SpawnEnemy();
		}
		enemiesText.text = ""+enemyAmount;
		UpdateEnemyTiles();
	}

	private void SpawnEnemy()
	{
		Vector3 location = new Vector3(Random.Range(0f,levelWidth),Random.Range(levelHeight,0f),0f);
		while(Vector3.Distance(location,playerTransform.position)<75f)
			location = new Vector3(Random.Range(0f,levelWidth),Random.Range(levelHeight,0f),0f);
		GameObject enemy = Instantiate(enemyPrefab, location,Quaternion.identity);
		enemy.name = "Enemy "+enemyList.Count;
		enemyList.Add(enemy);
	}

	public void UpdateEnemyTiles()
	{

		foreach(GameObject enemy in enemyList)
		{
			int closestTile = 0;
			float distance = enemy.transform.position.x - tileHandler.tiles[0].transform.position.x;
			distance = Mathf.Abs(distance);
			for(int i = 1;i<tileHandler.tiles.Count;i++)
			{
				float distanceToCheck = enemy.transform.position.x - tileHandler.tiles[i].transform.position.x;
				distanceToCheck = Mathf.Abs(distanceToCheck);
				if(distanceToCheck < distance)
				{
					closestTile = i;
					distance = distanceToCheck;
				}
				enemy.transform.SetParent(tileHandler.tiles[closestTile].transform);
			}		
		}
		RelocateNotActiveEnemies();
	}

	private void RelocateNotActiveEnemies()
	{
		foreach(GameObject enemy in enemyList)
		{
			if(!enemy.activeInHierarchy)
			{
				enemy.transform.localPosition = new Vector3(Random.Range(-125f,125f),enemy.transform.localPosition.y,enemy.transform.localPosition.z);
				enemy.transform.rotation = Quaternion.identity;
			}
		}
	}

	

}
