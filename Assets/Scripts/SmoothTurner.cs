﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothTurner : MonoBehaviour {

	public float turnSpeed = 1f;
	public float smoothTime = 1f;
	
	private float rotation = 90;
	public void TurnAround()
	{
		if(rotation == 90)
			rotation = -90;	
		else
			rotation = 90;
	}

	private void Update()
	{
		Quaternion targetRotation = Quaternion.Euler(0f, rotation, 0f);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * smoothTime);
	}
}
