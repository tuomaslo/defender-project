﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshHider : MonoBehaviour {

	public MeshRenderer[] meshRenderers;
	// Use this for initialization
	public void Hide()
	{
		for(int i = 0;i<meshRenderers.Length;i++)
		{
			meshRenderers[i].enabled = false;
		}
	}
}
