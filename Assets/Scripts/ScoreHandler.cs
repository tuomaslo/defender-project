﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreHandler : MonoBehaviour {

	public TextMeshProUGUI scoreText;
	private int score = 0;
	
	public void AddToScore(int i)
	{
		score+=i;
		UpdateScoreText();
	}

	private void UpdateScoreText()
	{
		scoreText.text = score+"";
	}
}
