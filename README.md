# Defender-project / Sub Defender
Author: Tuomas Louhelainen

This "Sub Defender" is a small game project based on legendary **Defender** made in a couple of days to demonstrate my ability to use Unity and my programming expertise.

![Sub Defender - game image](https://i.imgur.com/AJGW0vW.jpg)

## Links
 - [Youtube] Gameplay video link
 - [APK] Downloadable apk (Tested with OnePlus 3T)

## Gameplay
  - 2.5D side scroller arcade shooter
  - Objective is to destroy all remaining enemies to advance to next level
  - Enemies can be killed with weapon or ramming (ramming damages player)
  - Enemies target player and try to shoot you down, but their detection range is not good
  - Level consists of three different tiles and wraps around

## Assets used
  - [Micro sub 3d-model] used as model for players *ship*
  - [Mobile joystic] used as base, modified to suit needs
  - [Mobile Ocean] ocean graphics
  - [Particle pack] particles
  - [Rock pack] enviroment rocks
  - [Scifi drone] enemy model
  - [Post-processing stack]
  - Other assets imported but not necessary used

## Further development?
 - Sounds
 - Shield for player
 - Weapon powerups
 - New enemies / enemy behaviours
 - New / procedural level
 - Leaderboards

-----------------------------------------------------------------------
   [Micro sub 3d-model]: <https://assetstore.unity.com/packages/3d/vehicles/sea/micro-sub-77672>
   [Mobile joystic]: <https://github.com/ashwaniarya/Unity3D-Simple-Mobile-Joystick>
   [Mobile ocean]: <https://github.com/joaquingrech/mobileocean>
   [SonarFX]: <https://github.com/keijiro/SonarFx>
   [Particle pack]: <https://assetstore.unity.com/packages/vfx/particles/fork-particle-variety-pack-fx-96008>
   [Rock pack]: <https://assetstore.unity.com/packages/3d/environments/rock-boulders-2452>
   [Scifi drone]: <https://assetstore.unity.com/packages/3d/environments/rock-boulders-2452>
   [Post-processing stack]: <https://github.com/Unity-Technologies/PostProcessing>
   [Youtube]: <https://www.youtube.com/watch?v=nTJj4Ffseq0>
   [APK]: <https://drive.google.com/open?id=1lYjrF55P522ZjQCS3YZK18yli0wwV-uD>